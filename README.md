# sample-models

This repository contains sample code and notebooks for model training corresponding components in [AI-Library](https://gitlab.com/opendatahub/ai-library).

This also contains pre-trained models and datasets which serve as baseline models for evaluation of any models trained using the sample code. 