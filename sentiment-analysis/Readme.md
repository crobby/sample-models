This directory contains sample notebooks and trained models for sentiment analysis.

## Objective

Sentiment Analysis refers to the use of several techniques to systematically identify, extract, quantify, and study emotional states and subjective information. It aims to determine the attitudes, opinions, or emotional reactions of a speaker with respect to some topic. It can often be helpful in ascertaining the sentiment of a product or brand when given conversation data such as a social media feed or customer survey data.

The aim is to further integrate the model into [AI-Library](https://gitlab.com/opendatahub/ai-library) and make it available as a service for sentiment analysis of streaming and persistent data maintained in [Open Data Hub](https://www.opendatahub.io) infrastructure.


## Background

The [Open Data Hub](https://www.opendatahub.io) is an open source project developed by Red Hat for creating a machine-learning-as-a-service platform, built on OpenShift, using entirely open source components.

[AI-Library](https://gitlab.com/opendatahub/ai-library), a part of Open Data Hub is an Open Source collection of AI Components- machine learning algorithms, machine learning based solutions to common use-cases.


## Contents of the Directory

The notebooks present in this repository contain code to import a pre-trained BERT model and fine-tune it on a dataset for sentiment analysis task.

* `BERT-Train.ipynb`: Notebook used to train BERT based sentiment analysis model and save it to a directory
* `BERT-Test.ipynb`: Notebook used to test saved model and obtain predictions and evaluate labeled data in csv format
* `saved_model.pb` : Sentiment analysis model saved in Protocol Buffers format. 

## Model 

The model being trained here for sentiment analysis is based on BERT (Bidirectional Encoder Representation for transformers) which is an NLP pre-training technique.

BERT builds upon recent work in pre-training contextual representations — including Semi-supervised Sequence Learning, Generative Pre-Training, ELMo, and ULMFit. However, unlike these previous models, BERT is the first deeply bidirectional, unsupervised language representation, pre-trained using only a plain text corpus (in this case, Wikipedia).

The training workflow is split in two steps:

1. Unsupervised pre-training - Importing a pre-trained language model trained on unlabelled data to learn initial parameters of a neural net.
2. Supervised fine tuning - Adapt the obtained paramters to the target task using supervised objective and training it on labeled movie reviews dataset.

After a model has been trained, we test it on a validation dataset and verify the performance of the model and then run predictions on incoming new test dataset.

## Requirements to run the notebooks

For running these notebooks, and performing training of the models and running predictions we used the Jupyterhub installation of the [Open Data Hub](https://opendatahub.io/) which has GPU support. 

They can also be run on a local installation of Jupyter Notebooks and installing the neccesary requirements. Jupyter notebooks allow running code, documenting, visualization in the same environment which makes the process of training and prototyping more flexible. 

In order to install Jupyter Notebooks and the iPython Kernel, one can follow the steps given in the [documentation](https://jupyter.readthedocs.io/en/latest/install.html)

## Tutorials to train deep-learning models

These are some tutorials and walk-throughs for getting started with training deep learning models.

* For getting started on training neural networks using the Tensorflow API, one can go through this tutorial - [Train your first Neural network](https://www.tensorflow.org/tutorials/keras/basic_classification)
* For more information on pre-training and fine-tuning using BERT, one can follow this article on [Binary Text Classification](https://medium.com/swlh/a-simple-guide-on-using-bert-for-text-classification-bbf041ac8d04)